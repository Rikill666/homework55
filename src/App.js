import React, {Component} from 'react';
import Ingredients from "./components/Ingredients/Ingredients";
import Burger from "./components/Burger/Burger";
import nanoid from 'nanoid';

class App extends Component {
    state = {
        ingredients: [
            {name: 'Meat', count: 0, id: nanoid()},
            {name: 'Cheese', count: 0, id: nanoid()},
            {name: 'Salad', count: 0, id: nanoid()},
            {name: 'Bacon', count: 0, id: nanoid()}
        ]
    };

    deleteIngredient = id =>{
        const index = this.state.ingredients.findIndex(t => t.id === id);
        const ingredients = [...this.state.ingredients];
        ingredients[index].count--;
        this.setState({ingredients});
    };
    addIngredient = id =>{
        const index = this.state.ingredients.findIndex(t => t.id === id);
        const ingredients = [...this.state.ingredients];
        ingredients[index].count++;
        this.setState({ingredients});
    };

    render() {
        const divStyle = {
            display: 'flex',
            justifyContent: 'space-between',
            margin: '15px'
        };
        return (
            <div style={divStyle}>
                <Ingredients addClick={this.addIngredient} deleteClick={this.deleteIngredient} ingredients={this.state.ingredients}/>
                <Burger ingredientsList={this.state.ingredients}/>
            </div>
        );
    }
}

export default App;
