import React from 'react';
import IngredientsList from "../IngredientsList/IngredientsList";

const Ingredients = ({ingredients, deleteClick, addClick}) => {
    const divStyle = {
        width: '49%',
        border: '1px solid black',
        margin: '0',
        textAlign:'center'
    };
    return (
        <div style={divStyle}>
            <h5>Ingredients</h5>
            <ul>
                {ingredients.map(ingredient =>
                    <IngredientsList addClick={() => addClick(ingredient.id)} deleteClick={() => deleteClick(ingredient.id)} key={ingredient.id} ingredient={ingredient}/>
                )}
            </ul>
        </div>
    );
};

export default Ingredients;