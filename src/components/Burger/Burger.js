import React from 'react';
import "./Burger.css"
import Ingredient from "../Ingredient/Ingredient";
import Price from "../Price/Price";

const Burger = ({ingredientsList}) => {

    const fullIngredients = function(){
        let ingredients = [];
        ingredientsList.forEach(ingredient => {
            for (let i = 1; i <= ingredient.count; i++) {
                let component = {
                    ingredient: ingredient,
                    id: i
                };
                ingredients.push(component);
            }
        });
        return ingredients;
    };


    const divStyle = {
        width: '49%',
        height: 'auto',
        border: '1px solid black',
        textAlign: 'center'
    };

    return (
        <div style={divStyle}>
            <h5>Burger</h5>
            <div className="Burger">
                <div className="BreadTop">
                    <div className="Seeds1"/>
                    <div className="Seeds2"/>
                </div>
                {fullIngredients().map(ingredient => {
                    return <Ingredient key={ingredient.id + ingredient.ingredient.id} ingredient={ingredient.ingredient}/>
                })}
                <div className="BreadBottom"/>
            </div>
            <Price ingredientsList={ingredientsList}/>
        </div>
    );
};

export default Burger;