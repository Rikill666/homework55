import React from 'react';

const ingredientsPrices = {
    'Salad': 5,
    'Cheese': 20,
    'Meat': 50,
    'Bacon': 30
};
const Price = ({ingredientsList}) => {
    let price = 20;
    ingredientsList.forEach(function (ingredient){
        price += ((ingredientsPrices[ingredient.name]) * ingredient.count );
    });
    return (
        <p>
           Price: {price}
        </p>
    );
};

export default Price;