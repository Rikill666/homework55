import React from 'react';
import './IngredientsList.css'

const IngredientsList = ({ingredient, deleteClick, addClick}) => {
    const iconClassName = ingredient.count > 0 ? 'fas fa-trash-alt' : "fas";
    let addButtonIcon = "fas ";
    if(ingredient.name === 'Bacon'){
        addButtonIcon += "fa-bacon";
    }
    else if(ingredient.name === 'Meat'){
        addButtonIcon += "fa-drumstick-bite";
    }
    else if(ingredient.name === 'Cheese'){
        addButtonIcon += "fa-cheese";
    }
    else{
        addButtonIcon += "fa-spa";
    }

    const noAction = function () {};
    const iconAction = ingredient.count > 0 ? deleteClick : noAction ;
    return (
        <li>
            <i onClick={addClick} className={addButtonIcon}/>
            <p>{ingredient.name}</p>
            <p className='count'>x{ingredient.count} </p>
            <i onClick={iconAction} className={iconClassName}/>
        </li>
    );
};

export default IngredientsList;